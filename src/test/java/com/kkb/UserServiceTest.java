package com.kkb;

import com.kkb.pojo.User;
import com.kkb.service.UserService;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class UserServiceTest {
    private UserService userService;
    @Before
    public void init() {
        userService = new UserService();
    }
    @Test
    public void addUser() throws Exception {
        User user = new User();
        user.setName("宋江");
        user.setAge(29);
        user.setAddress("山东郓城");
        userService.addUser(user);
    }
    @Test
    public void getUserList() throws Exception {
        List<User> userList = userService.getUserList();
        userList.forEach(System.out::println);
    }
}
