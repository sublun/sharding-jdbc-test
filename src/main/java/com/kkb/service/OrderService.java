package com.kkb.service;

import com.kkb.common.ShardingDataSource;
import com.kkb.pojo.Order;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class OrderService {
    public boolean addOrderInfo(Order order) throws Exception{
        DataSource dataSource = ShardingDataSource.getInstance();
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("insert into t_order(order_id, user_id, info) values (?,?,?)");
        preparedStatement.setInt(1, order.getOrderId());
        preparedStatement.setInt(2, order.getUserId());
        preparedStatement.setString(3, order.getInfo());
        boolean result = preparedStatement.execute();
        return result;
    }

    public List<Order> findAll() throws Exception {
        DataSource dataSource = ShardingDataSource.getInstance();
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from t_order order by order_id");
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Order> orderList = new ArrayList<>();
        while(resultSet.next()) {
            Order order = new Order();
            order.setOrderId(resultSet.getInt("order_id"));
            order.setUserId(resultSet.getInt("user_id"));
            order.setInfo(resultSet.getString("info"));
            orderList.add(order);
        }
        return orderList;
    }

    public List<Order> findByUserId(int userId) throws Exception {
        DataSource dataSource = ShardingDataSource.getInstance();
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from t_order where user_id = ?");
        preparedStatement.setInt(1,userId);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Order> orderList = new ArrayList<>();
        while(resultSet.next()) {
            Order order = new Order();
            order.setOrderId(resultSet.getInt("order_id"));
            order.setUserId(resultSet.getInt("user_id"));
            order.setInfo(resultSet.getString("info"));
            orderList.add(order);
        }
        return orderList;
    }

    public Order findById(int orderId) throws Exception {
        DataSource dataSource = ShardingDataSource.getInstance();
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from t_order where order_id = ?");
        preparedStatement.setInt(1,orderId);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Order> orderList = new ArrayList<>();
        while(resultSet.next()) {
            Order order = new Order();
            order.setOrderId(resultSet.getInt("order_id"));
            order.setUserId(resultSet.getInt("user_id"));
            order.setInfo(resultSet.getString("info"));
            orderList.add(order);
        }
        if ( orderList.size() >= 1 ) {
            return orderList.get(0);
        }
        return null;
    }

    public List<Order> findByPage() throws Exception{
        DataSource dataSource = ShardingDataSource.getInstance();
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from t_order order by order_id limit 0,5");
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Order> orderList = new ArrayList<>();
        while(resultSet.next()) {
            Order order = new Order();
            order.setOrderId(resultSet.getInt("order_id"));
            order.setUserId(resultSet.getInt("user_id"));
            order.setInfo(resultSet.getString("info"));
            orderList.add(order);
        }
        return orderList;
    }
}
