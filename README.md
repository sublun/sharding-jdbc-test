# sharding-jdbc-test

#### 介绍
使用ShardingJDBC实现读写分离以及分库分表的例子

#### 软件架构
使用原生jdbc实现


#### 使用说明

OrderServiceTest：测试分库分表
UserServiceTest：测试读写分离


